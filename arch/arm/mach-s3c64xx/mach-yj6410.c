/*
 * Copyright 2008 Openmoko, Inc.
 * Copyright 2008 Simtec Electronics
 *	Ben Dooks <ben@simtec.co.uk>
 *	http://armlinux.simtec.co.uk/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
*/

#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/interrupt.h>
#include <linux/list.h>
#include <linux/timer.h>
#include <linux/init.h>
#include <linux/serial_core.h>
#include <linux/platform_device.h>
#include <linux/io.h>
#include <linux/i2c.h>
#include <linux/leds.h>
#include <linux/fb.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/smsc911x.h>
#include <linux/regulator/fixed.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/partitions.h>

#include <video/platform_lcd.h>

#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/irq.h>
#include <mach/hardware.h>
#include <mach/regs-fb.h>
#include <mach/map.h>

#include <asm/irq.h>
#include <asm/mach-types.h>

#include <plat/regs-serial.h>
#include <mach/regs-modem.h>
#include <mach/regs-gpio.h>
#include <mach/regs-sys.h>
#include <mach/regs-srom.h>
#include <mach/regs-clock.h>
#include <plat/iic.h>
#include <plat/fb.h>
#include <plat/gpio-cfg.h>
#include <plat/nand.h>

#include <mach/s3c6410.h>
#include <plat/clock.h>
#include <plat/devs.h>
#include <plat/cpu.h>
#include <plat/adc.h>
#include <plat/ts.h>
#include <linux/clk.h>


/* S3C_USB_CLKSRC 0: EPLL 1: CLK_48M */
#define S3C_USB_CLKSRC  1

#ifdef USB_HOST_PORT2_EN
#define OTGH_PHY_CLK_VALUE      (0x00)  /* Serial Interface, otg_phy input clk 48Mhz Oscillator */
//#define OTGH_PHY_CLK_VALUE      (0x60)  /* Serial Interface, otg_phy input clk 48Mhz Oscillator */
#else
#define OTGH_PHY_CLK_VALUE      (0x00)  /* UTMI Interface, otg_phy input clk 48Mhz Oscillator */
//#define OTGH_PHY_CLK_VALUE      (0x20)  /* UTMI Interface, otg_phy input clk 48Mhz Oscillator */
#endif

#define UCON S3C2410_UCON_DEFAULT | S3C2410_UCON_UCLK
#define ULCON S3C2410_LCON_CS8 | S3C2410_LCON_PNONE | S3C2410_LCON_STOPB
#define UFCON S3C2410_UFCON_RXTRIG8 | S3C2410_UFCON_FIFOMODE

static struct s3c2410_uartcfg smdk6410_uartcfgs[] __initdata = {
	[0] = {
		.hwport	     = 0,
		.flags	     = 0,
		.ucon	     = UCON,
		.ulcon	     = ULCON,
		.ufcon	     = UFCON,
	},
	[1] = {
		.hwport	     = 1,
		.flags	     = 0,
		.ucon	     = UCON,
		.ulcon	     = ULCON,
		.ufcon	     = UFCON,
	},
	[2] = {
		.hwport	     = 2,
		.flags	     = 0,
		.ucon	     = UCON,
		.ulcon	     = ULCON,
		.ufcon	     = UFCON,
	},
	[3] = {
		.hwport	     = 3,
		.flags	     = 0,
		.ucon	     = UCON,
		.ulcon	     = ULCON,
		.ufcon	     = UFCON,
	},
};

static struct mtd_partition yj6410_nand_part[] = {
	{
		.name       = "Bootloader",
		.offset     = 0,
		.size       = (256*SZ_1K),
		.mask_flags = MTD_CAP_NANDFLASH, //smlee; make this read-only
	},
	{
		.name       = "Kernel",
		.offset     = (256*SZ_1K),
		.size       = (4*SZ_1M) - (256*SZ_1K),
	},
	{
		.name       = "Root",
		.offset     = (4*SZ_1M), 
		.size       = (64*SZ_1M),
	},
	{
		.name       = "File System",
		.offset     = MTDPART_OFS_APPEND,
		.size       = MTDPART_SIZ_FULL,
	},

};

static struct s3c2410_nand_set yj6410_nand_sets[] = {
	[0] = {
		.name       = "nand",
		.nr_chips   = 1,
		.nr_partitions  = ARRAY_SIZE(yj6410_nand_part),
		.partitions = yj6410_nand_part,
	},
};

static struct s3c2410_platform_nand yj6410_nand_info = {
	.tacls      = 25,
	.twrph0     = 55,
	.twrph1     = 40,
	.nr_sets    = ARRAY_SIZE(yj6410_nand_sets),
	.sets       = yj6410_nand_sets,
};
/*
static struct resource smdk6410_smsc911x_resources_v1[] = {
	[0] = {
		.start = S3C64XX_PA_XM0CSN4,
		.end   = S3C64XX_PA_XM0CSN4 + SZ_1M - 1,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = S3C_EINT(2),
		.end   = S3C_EINT(2),

		.flags = IORESOURCE_IRQ | IRQ_TYPE_LEVEL_LOW,
	},
};
static struct smsc911x_platform_config smdk6410_smsc911x_pdata_v1 = {
	.irq_polarity  = SMSC911X_IRQ_POLARITY_ACTIVE_LOW,
	.irq_type      = SMSC911X_IRQ_TYPE_PUSH_PULL,
	.flags         = SMSC911X_USE_32BIT | SMSC911X_SAVE_MAC_ADDRESS,
	.phy_interface = PHY_INTERFACE_MODE_MII,
	.mac = { 0x00, 0x01 , 0x02 , 0x03 , 0x4 , 0x5},
};


static struct platform_device smdk6410_smsc911x_v1 = {
	.name          = "smsc911x",
	.id            = 0,
	.num_resources = ARRAY_SIZE(smdk6410_smsc911x_resources_v1),
	.resource      = &smdk6410_smsc911x_resources_v1[0],
	.dev = {
		.platform_data = &smdk6410_smsc911x_pdata_v1,
	},
};
*/

#ifdef CONFIG_SUPPORT_PORT0
static struct resource smdk6410_smsc911x_resources[] = {
	[0] = {
		.start = S3C64XX_PA_XM0CSN0,
		.end   = S3C64XX_PA_XM0CSN0 + SZ_1M - 1,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = S3C_EINT(0),
		.end   = S3C_EINT(0),

		.flags = IORESOURCE_IRQ | IRQ_TYPE_LEVEL_LOW,
	},
};
static struct smsc911x_platform_config smdk6410_smsc911x_pdata = {
	.irq_polarity  = SMSC911X_IRQ_POLARITY_ACTIVE_LOW,
	.irq_type      = SMSC911X_IRQ_TYPE_PUSH_PULL,
	.flags         = SMSC911X_USE_32BIT ,
	.phy_interface = PHY_INTERFACE_MODE_MII,
	.mac = { 0x00, 0x01 , 0x02 , 0x03 , 0x4 , 0x5},
};


static struct platform_device smdk6410_smsc911x = {
	.name          = "smsc911x",
	.id            =  1,
	.num_resources = ARRAY_SIZE(smdk6410_smsc911x_resources),
	.resource      = &smdk6410_smsc911x_resources[0],
	.dev = {
		.platform_data = &smdk6410_smsc911x_pdata,
	},
};

#endif

#ifdef CONFIG_SUPPORT_PORT1
static struct resource smdk6410_smsc911x_resources_1[] = {
	[0] = {
		.start = S3C64XX_PA_XM0CSN1,
		.end   = S3C64XX_PA_XM0CSN1 + SZ_1M - 1,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = S3C_EINT(1),
		.end   = S3C_EINT(1),

		.flags = IORESOURCE_IRQ | IRQ_TYPE_LEVEL_LOW,
	},
};
static struct smsc911x_platform_config smdk6410_smsc911x_pdata_1 = {
	.irq_polarity  = SMSC911X_IRQ_POLARITY_ACTIVE_LOW,
	.irq_type      = SMSC911X_IRQ_TYPE_PUSH_PULL,
	.flags         = SMSC911X_USE_32BIT ,
	.phy_interface = PHY_INTERFACE_MODE_MII,
};


static struct platform_device smdk6410_smsc911x_1= {
	.name          = "smsc911x",
	.id            = 2,
	.num_resources = ARRAY_SIZE(smdk6410_smsc911x_resources_1),
	.resource      = &smdk6410_smsc911x_resources_1[0],
	.dev = {
		.platform_data = &smdk6410_smsc911x_pdata_1,
	},
};
#endif
static int __init ethaddr_setup(char *line) {
	char *ep; int i; 
	for (i = 0; i < 6; i++) {

		smdk6410_smsc911x_pdata.mac [i] = line ? simple_strtoul(line, &ep, 16) : 0; 

		if (line) line = (*ep) ? ep+1 : ep; 
	} 

	printk("User MAC address: %pM\n", smdk6410_smsc911x_pdata.mac);
	return 0;

} __setup("ethaddr=", ethaddr_setup);



#if 0
static struct gpio_led ddc_led_list[] = {
	{
		.name = "led0",
		.default_trigger = "none",
		.gpio = S3C64XX_GPK(0),
		.active_low = 1,
	}, {
		.name = "led1",
		.default_trigger = "none",
		.gpio = S3C64XX_GPK(1),
		.active_low = 1,
	}, {
		.name = "led2",
		.default_trigger = "none",
		.gpio = S3C64XX_GPK(2),
		.active_low = 1,
	}, {
		.name = "led3",
		.default_trigger = "none",
		.gpio = S3C64XX_GPK(3),
		.active_low = 1,
	}, {
		.name = "led4",
		.default_trigger = "none",
		.gpio = S3C64XX_GPK(4),
		.active_low = 1,
	}, {
		.name = "led5",
		.default_trigger = "none",
		.gpio = S3C64XX_GPK(5),
		.active_low = 1,
	}, {
		.name = "led6",
		.default_trigger = "none",
		.gpio = S3C64XX_GPK(6),
		.active_low = 1,
	}, {
		.name = "led7",
		.default_trigger = "none",
		.gpio = S3C64XX_GPK(7),
		.active_low = 1,
	}, {
		.name = "led8",
		.default_trigger = "none",
		.gpio = S3C64XX_GPK(8),
		.active_low = 1,
	}, {
		.name = "led9",
		.default_trigger = "none",
		.gpio = S3C64XX_GPK(9),
		.active_low = 1,
	}, {
		.name = "led10",
		.default_trigger = "none",
		.gpio = S3C64XX_GPK(10),
		.active_low = 1,
	}, {
		.name = "led11",
		.default_trigger = "none",
		.gpio = S3C64XX_GPK(11),
		.active_low = 1,
	}, {
		.name = "led12",
		.default_trigger = "none",
		.gpio = S3C64XX_GPK(12),
		.active_low = 1,
	}, {
		.name = "led13",
		.default_trigger = "none",
		.gpio = S3C64XX_GPK(13),
		.active_low = 1,
	}, {
		.name = "led14",
		.default_trigger = "none",
		.gpio = S3C64XX_GPK(14),
		.active_low = 1,
	}, {
		.name = "led15",
		.default_trigger = "none",
		.gpio = S3C64XX_GPK(15),
		.active_low = 1,
	}, {
		.name = "led16",
		.default_trigger = "none",
		.gpio = S3C64XX_GPI(0),
		.active_low = 1,
	}, {
		.name = "led18",
		.default_trigger = "none",
		.gpio = S3C64XX_GPI(1),
		.active_low = 1,
	}, {
		.name = "led19",
		.default_trigger = "none",
		.gpio = S3C64XX_GPI(2),
		.active_low = 1,
	},

};
#else
static struct gpio_led ddc_led_list[] = {
	{
		.name = "led0",
		.default_trigger = "none",
		.gpio = S3C64XX_GPI(0),
		.active_low = 1,
	}, {
		.name = "led1",
		.default_trigger = "none",
		.gpio = S3C64XX_GPI(1),
		.active_low = 1,
	}, {
		.name = "led2",
		.default_trigger = "none",
		.gpio = S3C64XX_GPI(2),
		.active_low = 1,
	}, {
		.name = "led3",
		.default_trigger = "none",
		.gpio = S3C64XX_GPI(3),
		.active_low = 1,
	}, {
		.name = "led4",
		.default_trigger = "none",
		.gpio = S3C64XX_GPQ(2),
		.active_low = 1,
	}, {
		.name = "led5",
		.default_trigger = "none",
		.gpio = S3C64XX_GPQ(3),
		.active_low = 1,
	}, {
		.name = "led6",
		.default_trigger = "none",
		.gpio = S3C64XX_GPQ(4),
		.active_low = 1,
	}, 
};

#endif //led config




static struct gpio_led_platform_data ddc_led_pdata = {
	.num_leds = ARRAY_SIZE(ddc_led_list),
	.leds = ddc_led_list,
};

static struct platform_device ddc_leds = {
	.name = "leds-gpio",
	.id = -1,
	.dev = {
		.platform_data  = &ddc_led_pdata,
	},
};

static void __init link_led_init(void) {
	gpio_request(S3C64XX_GPI(0), "0");
	gpio_request(S3C64XX_GPI(1), "1");
	gpio_request(S3C64XX_GPI(2), "2");
	gpio_request(S3C64XX_GPI(3), "3");
	gpio_request(S3C64XX_GPQ(2), "4");
	gpio_request(S3C64XX_GPQ(3), "5");
	gpio_request(S3C64XX_GPQ(4), "6");
	//gpio_request(S3C64XX_GPK(7), "7");
	//gpio_request(S3C64XX_GPK(8), "8");
	//gpio_request(S3C64XX_GPK(9), "9");
	//	gpio_request(S3C64XX_GPK(10), "10");
	//	gpio_request(S3C64XX_GPK(11), "11");

	s3c_gpio_cfgpin(S3C64XX_GPI(0), S3C_GPIO_OUTPUT);
	s3c_gpio_cfgpin(S3C64XX_GPI(1), S3C_GPIO_OUTPUT);
	s3c_gpio_cfgpin(S3C64XX_GPI(2), S3C_GPIO_OUTPUT);
	s3c_gpio_cfgpin(S3C64XX_GPI(3), S3C_GPIO_OUTPUT);
	s3c_gpio_cfgpin(S3C64XX_GPQ(2), S3C_GPIO_OUTPUT);
	s3c_gpio_cfgpin(S3C64XX_GPQ(3), S3C_GPIO_OUTPUT);
	s3c_gpio_cfgpin(S3C64XX_GPQ(4), S3C_GPIO_OUTPUT);
	//s3c_gpio_cfgpin(S3C64XX_GPK(7), S3C_GPIO_OUTPUT);
	//s3c_gpio_cfgpin(S3C64XX_GPK(8), S3C_GPIO_OUTPUT);
	//s3c_gpio_cfgpin(S3C64XX_GPK(9), S3C_GPIO_OUTPUT);
	//	s3c_gpio_cfgpin(S3C64XX_GPK(10), S3C_GPIO_OUTPUT);
	//	s3c_gpio_cfgpin(S3C64XX_GPK(11), S3C_GPIO_OUTPUT);
	gpio_direction_output(S3C64XX_GPI(0), 1);
	gpio_direction_output(S3C64XX_GPI(1), 1);
	gpio_direction_output(S3C64XX_GPI(2), 1);
	gpio_direction_output(S3C64XX_GPI(3), 1);
	gpio_direction_output(S3C64XX_GPQ(2), 1);
	gpio_direction_output(S3C64XX_GPQ(3), 1);
	gpio_direction_output(S3C64XX_GPQ(4), 1);
}


static void __init sddc_gpio_init(void)
{
	gpio_request(S3C64XX_GPN(11), "PowerLineMonitor");		//sys/class/gpio/gpio155
	s3c_gpio_cfgpin(S3C64XX_GPN(11), S3C_GPIO_INPUT);
	s3c_gpio_setpull(S3C64XX_GPN(11), S3C_GPIO_PULL_UP);
	gpio_direction_input(S3C64XX_GPN(11));
	gpio_export(S3C64XX_GPN(11), 0);
	
	gpio_request(S3C64XX_GPF(7), "24VMON_1");		//sys/class/gpio/gpio45
	s3c_gpio_cfgpin(S3C64XX_GPF(7), S3C_GPIO_INPUT);
	s3c_gpio_setpull(S3C64XX_GPF(7), S3C_GPIO_PULL_UP);
	gpio_direction_input(S3C64XX_GPF(7));
	gpio_export(S3C64XX_GPF(7), 0);
	
	gpio_request(S3C64XX_GPF(8), "24VMON_2");		//sys/class/gpio/gpio46
	s3c_gpio_cfgpin(S3C64XX_GPF(8), S3C_GPIO_INPUT);
	s3c_gpio_setpull(S3C64XX_GPF(8), S3C_GPIO_PULL_UP);
	gpio_direction_input(S3C64XX_GPF(8));
	gpio_export(S3C64XX_GPF(8), 0);
}


static struct map_desc smdk6410_iodesc[] = {
};



static struct platform_device *smdk6410_devices[] __initdata = {
#ifdef CONFIG_SMDK6410_SD_CH0
	&s3c_device_hsmmc0,
#endif
#ifdef CONFIG_SMDK6410_SD_CH1
	&s3c_device_hsmmc1,
#endif
	&s3c_device_i2c0,
	&s3c_device_nand,
	&s3c_device_usb_hsotg,
	&s3c_device_ohci,
	&s3c_device_rtc,
/*	&smdk6410_smsc911x_v1,
#ifdef CONFIG_SUPPORT_PORT2
	&smdk6410_smsc911x,
#endif
#ifdef CONFIG_SUPPORT_PORT3
	&smdk6410_smsc911x_3,
#endif
#ifdef CONFIG_SUPPORT_PORT4
	&smdk6410_smsc911x_4,
#endif
*/
#ifdef CONFIG_SUPPORT_PORT0
	&smdk6410_smsc911x,
#endif
#ifdef CONFIG_SUPPORT_PORT1
	&smdk6410_smsc911x_1,
#endif
	&s3c_device_wdt,
	&s3c_device_adc,
	&ddc_leds,
};

static struct i2c_board_info i2c_devs0[] __initdata = {
	{ I2C_BOARD_INFO("24c08", 0x50), },
};

static struct i2c_board_info i2c_devs1[] __initdata = {
	{ I2C_BOARD_INFO("24c128", 0x57), },	/* Samsung S524AD0XD1 */
};


static struct s3c_adc_mach_info s3c_adc_platform = {
	/* s3c6410 support 12-bit resolution */
	.delay  =   10000,
	.presc  =   49,
	.resolution =   12,
};


static void __init smdk6410_map_io(void)
{

	s3c64xx_init_io(smdk6410_iodesc, ARRAY_SIZE(smdk6410_iodesc));
	s3c24xx_init_clocks(12000000);
	s3c24xx_init_uarts(smdk6410_uartcfgs, ARRAY_SIZE(smdk6410_uartcfgs));

}


/* USB2.0 OTG Controller register */
#define S3C_USBOTG_PHYREG(x)        ((x) + S3C_VA_USB_HSPHY)
#define S3C_USBOTG_PHYPWR       S3C_USBOTG_PHYREG(0x0)
#define S3C_USBOTG_PHYCLK       S3C_USBOTG_PHYREG(0x4)
#define S3C_USBOTG_RSTCON       S3C_USBOTG_PHYREG(0x8)


/* Initializes OTG Phy. */
void otg_phy_init(void) {

	writel(readl(S3C_OTHERS)|S3C_OTHERS_USB_SIG_MASK, S3C_OTHERS);
	writel(0x0, S3C_USBOTG_PHYPWR);     /* Power up */
	writel(OTGH_PHY_CLK_VALUE, S3C_USBOTG_PHYCLK);
	writel(0x1, S3C_USBOTG_RSTCON);

	udelay(50);
	writel(0x0, S3C_USBOTG_RSTCON);
	udelay(50);
}

void usb_host_clk_en(void) {
	struct clk *otg_clk;

	switch (S3C_USB_CLKSRC) {
		case 0: /* epll clk */
			writel((readl(S3C_CLK_SRC)& ~S3C6400_CLKSRC_UHOST_MASK)
					|S3C_CLKSRC_EPLL_CLKSEL|S3C_CLKSRC_UHOST_EPLL,
					S3C_CLK_SRC);

			/* USB host clock divider ratio is 2 */
			writel((readl(S3C_CLK_DIV1)& ~S3C6400_CLKDIV1_UHOST_MASK)
					|(1<<20), S3C_CLK_DIV1);
			break;
		case 1: /* oscillator 48M clk */
			otg_clk = clk_get(NULL, "otg");
			clk_enable(otg_clk);
			writel(readl(S3C_CLK_SRC)& ~S3C6400_CLKSRC_UHOST_MASK, S3C_CLK_SRC);
			otg_phy_init();

			/* USB host clock divider ratio is 1 */
			writel(readl(S3C_CLK_DIV1)& ~S3C6400_CLKDIV1_UHOST_MASK, S3C_CLK_DIV1);
			break;
		default:
			printk(KERN_INFO "Unknown USB Host Clock Source\n");
			BUG();
			break;
	}

	writel(readl(S3C_HCLK_GATE)|S3C_CLKCON_HCLK_UHOST|S3C_CLKCON_HCLK_SECUR,
			S3C_HCLK_GATE);
	writel(readl(S3C_SCLK_GATE)|S3C_CLKCON_SCLK_UHOST, S3C_SCLK_GATE);

}


static void __init usb_otg_init(void)
{
	printk(KERN_INFO "USB OTG Hardware Initializing\n");

	gpio_request(S3C64XX_GPN(6), "USB Power");

	// Power Enable
	s3c_gpio_cfgpin(S3C64XX_GPN(6), S3C_GPIO_OUTPUT);
	s3c_gpio_setpull(S3C64XX_GPN(6), S3C_GPIO_PULL_NONE);
	gpio_direction_output(S3C64XX_GPN(6), 1);
}



static void __init smdk6410_machine_init(void)
{


	s3c_i2c0_set_platdata(NULL);

	s3c_adc_set_platdata(&s3c_adc_platform);

	s3c_nand_set_platdata(&yj6410_nand_info);

//	i2c_register_board_info(0, i2c_devs0, ARRAY_SIZE(i2c_devs0));
//	i2c_register_board_info(1, i2c_devs1, ARRAY_SIZE(i2c_devs1));

	platform_add_devices(smdk6410_devices, ARRAY_SIZE(smdk6410_devices));

	usb_otg_init();

	usb_host_clk_en();
	
	//link_led_init(); //SMLEE: Never Use This Function, because it'll break down the /sys/class/leds
	sddc_gpio_init();
}

MACHINE_START(YJ6410, "YJ6410")
	/* Maintainer: Ben Dooks <ben-linux@fluff.org> */
	.phys_io	= S3C_PA_UART & 0xfff00000,
	.io_pg_offst	= (((u32)S3C_VA_UART) >> 18) & 0xfffc,
	.boot_params	= S3C64XX_PA_SDRAM + 0x100,

	.init_irq	= s3c6410_init_irq,
	.map_io		= smdk6410_map_io,
	.init_machine	= smdk6410_machine_init,
	.timer		= &s3c24xx_timer,
MACHINE_END
