/* linux/arch/arm/plat-s3c64xx/adc.c
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Copyright (c) 2004 Arnaud Patard <arnaud.patard@rtp-net.org>
 * iPAQ H1940 touchscreen support
 *
 * ChangeLog
 *
 * 2004-09-05: Herbert Pötzl <herbert@13thfloor.at>
 *	- added clock (de-)allocation code
 *
 * 2005-03-06: Arnaud Patard <arnaud.patard@rtp-net.org>
 *      - h1940_ -> s3c24xx (this driver is now also used on the n30
 *        machines :P)
 *      - Debug messages are now enabled with the config option
 *        TOUCHSCREEN_S3C_DEBUG
 *      - Changed the way the value are read
 *      - Input subsystem should now work
 *      - Use ioremap and readl/writel
 *
 * 2005-03-23: Arnaud Patard <arnaud.patard@rtp-net.org>
 *      - Make use of some undocumented features of the touchscreen
 *        controller
 *
 */


#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/input.h>
#include <linux/init.h>
#include <linux/serio.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/miscdevice.h>
#include <linux/clk.h>
#include <linux/mutex.h>

#include <asm/io.h>
#include <asm/irq.h>
#include <mach/hardware.h>
#include <asm/uaccess.h>

#include <plat/regs-adc.h>
#include <plat/adc.h>
#include <mach/irqs.h>

//YJSOFT ADC Driver 

#define S3C_ADCREG(x)           (x)

#define S3C_ADCCON          S3C_ADCREG(0x00)
#define S3C_ADCTSC          S3C_ADCREG(0x04)
#define S3C_ADCDLY          S3C_ADCREG(0x08)
#define S3C_ADCDAT0         S3C_ADCREG(0x0C)
#define S3C_ADCDAT1         S3C_ADCREG(0x10)
#define S3C_ADCUPDN         S3C_ADCREG(0x14)
#define S3C_ADCCLRINT           S3C_ADCREG(0x18)
#define S3C_ADCMUX          S3C_ADCREG(0x1C)
#define S3C_ADCCLRWK            S3C_ADCREG(0x20)




/* ADCCON Register Bits */
#define S3C_ADCCON_RESSEL_10BIT     (0x0<<16)
#define S3C_ADCCON_RESSEL_12BIT     (0x1<<16)
#define S3C_ADCCON_ECFLG        (1<<15)
#define S3C_ADCCON_PRSCEN       (1<<14)
#define S3C_ADCCON_PRSCVL(x)        (((x)&0xFF)<<6)
#define S3C_ADCCON_PRSCVLMASK       (0xFF<<6)
#define S3C_ADCCON_SELMUX(x)        (((x)&0x7)<<3)
#define S3C_ADCCON_SELMUX_1(x)      (((x)&0xF)<<0)
#define S3C_ADCCON_MUXMASK      (0x7<<3)
#define S3C_ADCCON_RESSEL_10BIT_1   (0x0<<3)
#define S3C_ADCCON_RESSEL_12BIT_1   (0x1<<3)
#define S3C_ADCCON_STDBM        (1<<2)
#define S3C_ADCCON_READ_START       (1<<1)
#define S3C_ADCCON_ENABLE_START     (1<<0)
#define S3C_ADCCON_STARTMASK        (0x3<<0)


/* ADCTSC Register Bits */
#define S3C_ADCTSC_UD_SEN       (1<<8)
#define S3C_ADCTSC_YM_SEN       (1<<7)
#define S3C_ADCTSC_YP_SEN       (1<<6)
#define S3C_ADCTSC_XM_SEN       (1<<5)
#define S3C_ADCTSC_XP_SEN       (1<<4)
#define S3C_ADCTSC_PULL_UP_DISABLE  (1<<3)
#define S3C_ADCTSC_AUTO_PST     (1<<2)
#define S3C_ADCTSC_XY_PST(x)        (((x)&0x3)<<0)

/* ADCDAT0 Bits */
#define S3C_ADCDAT0_UPDOWN      (1<<15)
#define S3C_ADCDAT0_AUTO_PST        (1<<14)
#define S3C_ADCDAT0_XY_PST      (0x3<<12)
#define S3C_ADCDAT0_XPDATA_MASK     (0x03FF)
#define S3C_ADCDAT0_XPDATA_MASK_12BIT   (0x0FFF)

#define ADC_MINOR 	131
#define ADC_INPUT_PIN   _IOW('S', 0x0c, unsigned long)

//#define ADC_WITH_TOUCHSCREEN

static struct clk	*adc_clock;

static void __iomem 	*base_addr;
static int adc_port =  1;
struct s3c_adc_mach_info *plat_data;


#ifdef ADC_WITH_TOUCHSCREEN
static DEFINE_MUTEX(adc_mutex);

static unsigned long data_for_ADCCON;
static unsigned long data_for_ADCTSC;

static void s3c_adc_save_SFR_on_ADC(void)
{
	data_for_ADCCON = readl(base_addr + S3C_ADCCON);
	data_for_ADCTSC = readl(base_addr + S3C_ADCTSC);
}

static void s3c_adc_restore_SFR_on_ADC(void)
{
	writel(data_for_ADCCON, base_addr + S3C_ADCCON);
	writel(data_for_ADCTSC, base_addr + S3C_ADCTSC);
}
#else
static struct resource	*adc_mem;
#endif

static int s3c_adc_open(struct inode *inode, struct file *file)
{
//	printk(KERN_INFO " s3c_adc_open() entered\n");
	return 0;
}

unsigned int s3c_adc_convert_char(void)
{
	unsigned int adc_return = 0;
	unsigned long data0;
	unsigned long data1;
	unsigned long tempReg; //smlee added

	tempReg = readl(base_addr + S3C_ADCCON);
	tempReg = tempReg & (~(0x7 << 3));
	tempReg |= S3C_ADCCON_SELMUX(adc_port);
	writel(tempReg, base_addr + S3C_ADCCON);

	//writel(readl(base_addr + S3C_ADCCON) | S3C_ADCCON_SELMUX(adc_port), base_addr + S3C_ADCCON);
	udelay(10);

	writel(readl(base_addr + S3C_ADCCON) | S3C_ADCCON_ENABLE_START, base_addr + S3C_ADCCON);

	do {
		data0 = readl(base_addr + S3C_ADCCON);
	} while(!(data0 & S3C_ADCCON_ECFLG));

	data1 = readl(base_addr + S3C_ADCDAT0);

	if (plat_data->resolution == 12)
		adc_return = data1 & S3C_ADCDAT0_XPDATA_MASK_12BIT;
	else
		adc_return = data1 & S3C_ADCDAT0_XPDATA_MASK;

	return adc_return;
}


static ssize_t
ioctl_s3c_adc_read(struct file *file, char __user * buffer,
		size_t size, loff_t * pos)
{
	int  adc_value = 0;

//	printk(KERN_INFO " s3c_adc_read() entered\n");

#ifdef ADC_WITH_TOUCHSCREEN
        mutex_lock(&adc_mutex);
	s3c_adc_save_SFR_on_ADC();
#endif

	adc_value = s3c_adc_convert_char();

#ifdef ADC_WITH_TOUCHSCREEN
	s3c_adc_restore_SFR_on_ADC();
	mutex_unlock(&adc_mutex);
#endif

//	printk(KERN_INFO " Converted Value: %03d\n", adc_value);

	if (copy_to_user(buffer, &adc_value, sizeof(unsigned int))) {
		return -EFAULT;
	}
	return sizeof(unsigned int);
}


static int s3c_adc_ioctl(struct inode *inode, struct file *file,
	unsigned int cmd, unsigned long arg)
{

//       printk(KERN_INFO " s3c_adc_ioctl(cmd:: %d) entered, ADC_INPUT_PIN=%d, adc_port=%d,arg=%d\n", cmd, ADC_INPUT_PIN,adc_port, arg);

	switch (cmd) {
		case ADC_INPUT_PIN:
			adc_port = (unsigned int) arg;

			if (adc_port >= 4)
				printk(" %d is already reserved for TouchScreen\n", adc_port);
			else 
				; //smlee printk("adc_port=%d\n", adc_port) ; 

			return 0;

              default:
			return -ENOIOCTLCMD;
	}
}
 
static struct file_operations s3c_adc_fops = {
	.owner		= THIS_MODULE,
	.read		= ioctl_s3c_adc_read,
	.open		= s3c_adc_open,
	.ioctl		= s3c_adc_ioctl,
};

static struct miscdevice s3c_adc_miscdev = {
	.minor		= ADC_MINOR,
	.name		= "adc",
	.fops		= &s3c_adc_fops,
};

static struct s3c_adc_mach_info *s3c_adc_get_platdata(struct device *dev)
{
	if(dev->platform_data != NULL)
	{
		return (struct s3c_adc_mach_info*) dev->platform_data;
	} else {
		printk(KERN_INFO "No ADC platform data \n");
		return 0;
	}
}

/*
 * The functions for inserting/removing us as a module.
 */

static int __init s3c_adc_probe(struct platform_device *pdev)
{
	struct resource	*res;
	struct device *dev;
	int ret;
	int size;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	dev = &pdev->dev;

	if(res == NULL){
		printk("no memory resource specified\n");
		return -ENOENT;
	}

	size = (res->end - res->start) + 1;

#if !defined(ADC_WITH_TOUCHSCREEN)
	adc_mem = request_mem_region(res->start, size, pdev->name);
	if(adc_mem == NULL){
		printk("failed to get memory region\n");
		ret = -ENOENT;
		goto err_req;
	}
#endif

	base_addr = ioremap(res->start, size);
	if(base_addr ==  NULL){
		printk("ADC fail to ioremap() region\n");
		ret = -ENOENT;
		goto err_map;
	}

	adc_clock = clk_get(&pdev->dev, "adc");

	if(IS_ERR(adc_clock)){
		printk("ADC failed to fine ADC clock source\n");
		ret = PTR_ERR(adc_clock);
		goto err_clk;
	}

	clk_enable(adc_clock);

	/* read platform data from device struct */
	plat_data = s3c_adc_get_platdata(&pdev->dev);

	if ((plat_data->presc & 0xff) > 0)
		writel(S3C_ADCCON_PRSCEN | S3C_ADCCON_PRSCVL(plat_data->presc & 0xff), base_addr + S3C_ADCCON);
	else
		writel(0, base_addr + S3C_ADCCON);

	/* Initialise registers */
	if ((plat_data->delay & 0xffff) > 0)
		writel(plat_data->delay & 0xffff, base_addr + S3C_ADCDLY);

	if (plat_data->resolution == 12)
		writel(readl(base_addr + S3C_ADCCON) | S3C_ADCCON_RESSEL_12BIT, base_addr + S3C_ADCCON);

	ret = misc_register(&s3c_adc_miscdev);
	if (ret) {
		printk (KERN_ERR "cannot register miscdev on minor=%d (%d)\n",
			ADC_MINOR, ret);
		goto err_clk;
	}

	printk("S3C64XX ADC driver successfully probed\n");

	return 0;

err_clk:
	clk_disable(adc_clock);
	clk_put(adc_clock);

err_map:
	iounmap(base_addr);

#if !defined(ADC_WITH_TOUCHSCREEN)
err_req:
	release_resource(adc_mem);
	kfree(adc_mem);
#endif

	return ret;
}


static int s3c_adc_remove(struct platform_device *dev)
{
	printk(KERN_INFO "s3c_adc_remove() of ADC called !\n");
	return 0;
}

#ifdef CONFIG_PM
static unsigned int adccon, adctsc, adcdly;

static int s3c_adc_suspend(struct platform_device *dev, pm_message_t state)
{
	adccon = readl(base_addr + S3C_ADCCON);
	adctsc = readl(base_addr + S3C_ADCTSC);
	adcdly = readl(base_addr + S3C_ADCDLY);

	clk_disable(adc_clock);

	return 0;
}

static int s3c_adc_resume(struct platform_device *pdev)
{
	clk_enable(adc_clock);

	writel(adccon, base_addr + S3C_ADCCON);
	writel(adctsc, base_addr + S3C_ADCTSC);
	writel(adcdly, base_addr + S3C_ADCDLY);

	return 0;
}
#else
#define s3c_adc_suspend NULL
#define s3c_adc_resume  NULL
#endif

static struct platform_driver s3c_adc_driver = {
       .probe          = s3c_adc_probe,
       .remove         = s3c_adc_remove,
       .suspend        = s3c_adc_suspend,
       .resume         = s3c_adc_resume,
       .driver		= {
		.owner	= THIS_MODULE,
		.name	= "s3c-adc",
	},
};

static char banner[] __initdata = KERN_INFO "S3C64XX ADC driver, (c) 2008 Samsung Electronics\n";

int __init s3c_adc_init(void)
{
	int ret = 0;
	printk(banner);
	ret = platform_driver_register(&s3c_adc_driver);
	if (ret)
		        printk(KERN_ERR "%s: failed to add adc driver\n", __func__);

	return ret;
}

void __exit s3c_adc_exit(void)
{
	    platform_driver_unregister(&s3c_adc_driver);
}

module_init(s3c_adc_init);
module_exit(s3c_adc_exit);


MODULE_DESCRIPTION("S3C64XX ADC driver");
MODULE_LICENSE("GPL");

